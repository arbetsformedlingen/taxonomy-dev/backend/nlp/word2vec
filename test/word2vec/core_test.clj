(ns word2vec.core-test
  (:require [clojure.test :refer :all]
            [word2vec.core :refer :all]
            [word2vec.ling :as ling]
            ))


(deftest test-process-head
  (testing "empty period"
    (is (= [[] ""] (ling/process-head [] "" "")  ))
    (is (= [[] ""] (ling/process-head [] "" " ")  ))
    (is (= [["."] ""] (ling/process-head [] "" ".")  ))
    (is (= [[] "a"] (ling/process-head [] "" "a")  ))
    (is (= [["a"] ""] (ling/process-head [] "a" " ")  ))
    (is (= [["a"] "!"] (ling/process-head [] "a" "!")  ))
    )
  )






#_(comment

  (def test-tokens (tokenize-text test-text))
  (def test-token-index (create-token-index test-tokens))
  (def test-sentences (create-sentences-from-tokens test-tokens))
  (def test-word-corpus-matrix  (context-tokens-to-vectors (sort (keys test-token-index))  (token-to-one-hot-vector-fn test-token-index))) ;; TODO make an identity matrix with the same length as the word-corpus

  (def test-center-word-with-contexts (create-contexts-from-sentences test-sentences 3))

  (def test-center-word-and-context-as-vectors (map #(context-and-center-word-as-vectors % test-token-index) test-center-word-with-contexts))


  (def a-test-center-word-with-contexts (first test-center-word-with-contexts))
  (def a-test-center-word-as-vector (word-vector-from-token (first a-test-center-word-with-contexts) test-token-index )))




#_(def x-train-test-data (dge  (mapcat seq test-average-contexts))) ;; TODO fix hack
#_(def y-train-test-data (dge (map :center-word-vector test-center-word-and-context-as-vectors)))
