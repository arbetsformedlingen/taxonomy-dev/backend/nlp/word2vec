(ns word2vec.word2vec
  (:require
            [clojure.core :refer [cond->]]
            [clojure.string :as string]
            [uncomplicate.commons.core :refer [let-release with-release]]
            [uncomplicate.diamond.dnn
             :refer
             [cost fully-connected init! network train]]
            [uncomplicate.diamond.internal.cudnn.factory :refer [cudnn-factory]]
            [uncomplicate.diamond.tensor :refer [batcher input tensor]]
            [uncomplicate.neanderthal.core
             :refer

             [dim
              dot
              ;;              entry!
              mm
              mrows
              ncols
              nrm2
              row
              scal
              scal!
              submatrix
              transfer!
              view-vctr
              xpy
              ge
              native
              native!
              view-ge
              axpy!
              copy!
              ]
             ]
            [uncomplicate.neanderthal.native :refer [fge fv dge fsp]]
            [uncomplicate.neanderthal.real :refer [entry!]]
            [uncomplicate.neanderthal.vect-math :refer [exp]]
            [clojure.core :refer [cond->]]
            [uncomplicate.diamond
             [tensor :refer [tensor transformer connector transformer
                             desc revert shape input output view-tz batcher with-diamond

                             ]]
             [native :refer [map-tensor]]
             [dnn :refer [fully-connected network init! train cost infer]]
             [metrics :refer [confusion-matrix contingency-totals classification-metrics]]]

            [uncomplicate.diamond.internal.protocols :refer [layers]]
            [uncomplicate.diamond.internal.protocols :refer [weights bias]]
            [clojure.java.io :as io]

            [clojure.core :refer [cond->]]
            [clojure.core :refer [cond->]]

            [uncomplicate.neanderthal.cuda :refer [cuge]]

            [uncomplicate.diamond.internal.cudnn.core :as cudnn
             :refer [tensor-descriptor cudnn-handle transform-tensor activation-descriptor
                     activation-forward activation-backward get-activation-descriptor]]
            )
  )

(comment
  (def wd "/home/ubuntu/repo/word2vec/resources/data/")
  (def tt (take 10 (word2vec.io/nippy-thaw-from-file (str wd "center-word-and-contexts.nippy")))))

(defn word-vector-from-token [token token-index-map]
  (let-release [x (fv (count token-index-map))]
               (entry! x (get token-index-map token) 1.0)))


(defn average-contexts
  "Creates a vector of the average word vectors from a context.
  Uses a zero vector to represent out of vocabulary tokens."
  [contexts token-index-map]
  (let [context-token-indexes (map #(token-index-map % 0) contexts)
        context-token-indexes-size (count context-token-indexes)
        context-token-indexes-and-row-numbers (map (fn [a b] [a b])
                                                   context-token-indexes
                                                   (range context-token-indexes-size))]
    (let-release [m (fge (count token-index-map) context-token-indexes-size )
                  ones-vector (fge (ncols m) 1 (repeat 1))  ;; used for averageing a matrix
                  ]
                 (doseq [[x y] context-token-indexes-and-row-numbers]
                   (entry! m x y 1.0))
                 (scal! (/ 1 context-token-indexes-size) (mm m ones-vector))
                 )))

(defn context-and-center-word-as-vectors [[center-word [left-context right-context]]
                                          token-index-map]
  {:center-word-vector (word-vector-from-token center-word token-index-map)
   :average-context-vector (average-contexts (lazy-cat left-context right-context) token-index-map )})



(defn transform-context-and-center-word-to-matrixes [context-and-center-words]
  (let [number-of-words (dim (:center-word-vector (first context-and-center-words)))
        number-of-examples (count context-and-center-words)
        ]
    (let-release [
                  average-contexts-matrix (fge number-of-words number-of-examples)
                  center-word-matrix (fge number-of-words number-of-examples)
                  ]

      ;; TODO implement copy values with submatrixes and entry!

      (doseq [[current-example-index {:keys [center-word-vector
                         average-context-vector
                         ]}] (map-indexed vector context-and-center-words)]

        (transfer! center-word-vector
                   (submatrix center-word-matrix  0  current-example-index number-of-words 1))
        (transfer! average-context-vector
                   (submatrix average-contexts-matrix 0 current-example-index number-of-words 1))
        )

      {:center-word-matrix center-word-matrix
       :average-contexts-matrix average-contexts-matrix
       }
      ))
  )

;;13
;; use (count token-index) to get number-of-words
;; Try symmetric packed matrix SP instead of fge
(defn transform-context-and-center-word-to-matrixes-2 [center-word-and-contexts-as-indexes number-of-words]
  (let [number-of-examples (count center-word-and-contexts-as-indexes)]

    (let-release
        [average-contexts-matrix (fge number-of-words number-of-examples)
         center-word-matrix (fge number-of-words number-of-examples)]
      (doseq [[idx item] (map-indexed vector center-word-and-contexts-as-indexes)]
        (if (first item)
          (let [center-word-index (first item)
                context-indexes (second item)
                ]
            (doseq [context-idx context-indexes]
              (if  context-idx
                (entry! average-contexts-matrix context-idx idx 0.25)) ;; TODO add if the same
              )

            (entry! center-word-matrix center-word-index idx 1.0)))
        )
      {:center-word-matrix center-word-matrix
       :average-contexts-matrix average-contexts-matrix
       }
      )
    )
  )

(defn train-word-vectors [fact x-train y-train x-test y-test batch-size]
  (let [train-examples-size (ncols x-train) ;; number of training examples
        vocab-size (mrows x-train)
        test-examples-size (ncols x-test)
        ]

    (with-release [x-tensor           (tensor fact [train-examples-size vocab-size] :float :nc)
                   x-minibatch-tensor (tensor fact [batch-size vocab-size] :float :nc)
                   y-tensor           (tensor fact [train-examples-size vocab-size] :float :nc)
                   y-minibatch-tensor (tensor fact [batch-size vocab-size] :float :nc)
                   net-blueprint      (network fact x-minibatch-tensor
                                               [(fully-connected [300] :relu)
                                                (fully-connected [vocab-size] :softmax)  ;; 52 word
                                                ])
                   net (init! (net-blueprint x-minibatch-tensor :adam)) ;; try :sgd
                   net-infer (net-blueprint x-minibatch-tensor)
                   crossentropy-cost (cost net y-minibatch-tensor :crossentropy)
                   x-batcher (batcher x-tensor (input net))
                   y-batcher (batcher y-tensor y-minibatch-tensor)]
      (transfer! x-train (view-vctr x-tensor))
      (transfer! y-train (view-vctr y-tensor))
      (time (train net x-batcher y-batcher crossentropy-cost 5 []))
      (transfer! net net-infer);; ?? why
      (with-release [x-test-tensor (tensor fact [test-examples-size vocab-size] :float :nc)
                     y-test-tensor (tensor fact [test-examples-size vocab-size] :float :nc)
                     _ (transfer! x-test (view-vctr x-test-tensor))
                     _ (transfer! y-test (view-vctr y-test-tensor))

                     infered-words (infer net-infer x-test-tensor)
                     native-infered-words (native infered-words)
                     l (layers net-infer)
                     layer-0 (l 0)
                     layer-1 (l 1)
                     w0 (view-ge (view-vctr (weights layer-0)) 300 vocab-size )
                     w1 (view-ge (view-vctr (weights layer-1)) 300 vocab-size )
                     out0 (fge 300 vocab-size)
                     out1 (fge 300 vocab-size)
                     ]
        (transfer! w0  out0)
        (transfer! w1  out1)
        (axpy! 1.0 out0 out1)
        {:metrics (classification-metrics (native y-test-tensor) native-infered-words)
         :model (copy! out1 (fge 300 vocab-size))
           }
        ))))
