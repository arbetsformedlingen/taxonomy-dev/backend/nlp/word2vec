(ns word2vec.core
  (:gen-class)
  (:require
   [word2vec.build :as build]
            ))


(defn create-tokens [workdir number-of-documents-to-process]
  (if number-of-documents-to-process
    (println "Limiting training data to only use "
             number-of-documents-to-process
             " documents.")
    (println "Using all ads in training data"))
  (build/create-tokens-from-job-ads! workdir (when number-of-documents-to-process
                                       (Long/valueOf number-of-documents-to-process)))
  )


(defn process-args [args]
  (case (first args)
    "token" (create-tokens (second args) (nth args 2 nil))
    "token-index" (build/create-token-index (second args))
    "context" (build/create-contexts-from-sentences-and-center-word (second args) )
    "w2v" (build/train-word-vectors! (second args) )
    ;;"all" (process-all (second args) (nth args 2 nil))
    )
  )

(defn -main
  "Start with `lein run token workdir/`

  The first arg is what program you want to use.
  The second arg is the working directory, where the data should be saved.
  The workdir must end with a trailing / !!!
   The third args is used if you want to limit how many ads will be processed"
  [& args]
  (do
    (println "Starting program")
    (process-args args)
    (System/exit 0)
    )
  )
