(ns word2vec.build
  (:require
   [clojure.core.reducers :as r]
   [word2vec.document-parser :as dp]
   [word2vec.word2vec :as w2v]
   [uncomplicate.commons.core :refer [let-release with-release]]
   [uncomplicate.diamond.internal.cudnn.factory :refer [cudnn-factory]]
   [clojure.java.io :as io]
   [word2vec.io :as wio]
   [word2vec.ling :as ling]
   [clojure.set :as s]
  [clojure.core.reducers :as r]
   [uncomplicate.neanderthal.core
    :refer [dot  nrm2 cols col rows row]]))

;; (def wd "/home/ubuntu/repo/word2vec/resources/data/")

(def tokenized-text-filename "tokenized-text.nippy")
(def token-index-filename "token-index.nippy")
(def sentences-filename "sentences.txt")
(def center-word-and-contexts-filename "center-word-and-contexts.nippy")

(def training-and-test-data-file-name "training-and-test-data.nippy")

(def model-filename "model.nippy")
(def metrics-filename "metrics.edn")

;; TODO refactor , create a folder for a run, create subfolders for senteces etc. write to a lot of separate files.
(defn create-tokens-from-job-ads! [workdir number-of-ads]
  (let [ tokens (if number-of-ads
                 (dp/parse-job-ads-to-tokens (take number-of-ads (dp/load-job-ads)))
                 (dp/parse-job-ads-to-tokens (dp/load-job-ads)))
        ]
    (wio/nippy-freeze-to-file! tokens (str workdir tokenized-text-filename))))


#_(defn create-sentences-from-tokens [workdir]
  (wio/save-lists-of-list-as-space-separated-rows
     (:sentences (into {}
                       (r/reduce ling/sentence-reduce-fn {:sentences [] :buffer []}
                                 (wio/load-lazy-list (str workdir tokens-filename)))))
                         (str workdir sentences-filename))
  )


(def token-index-size 40000) ;; I can't create larger matrices

(defn create-token-index [workdir]
  ;; save all unique tokens in a textfile
  (wio/nippy-freeze-to-file!
   (ling/create-token-index  (wio/nippy-thaw-from-file (str workdir tokenized-text-filename))
                             token-index-size)
   (str workdir token-index-filename)
   )
  )

(defn create-contexts-from-sentences-and-center-word [workdir]
  (let [token-index (wio/nippy-thaw-from-file (str workdir token-index-filename))
        tokenized-text (wio/nippy-thaw-from-file (str workdir tokenized-text-filename))
        ]
    (wio/nippy-freeze-to-file!
     (dp/tokenized-text-to-center-word-and-contexts tokenized-text token-index)
        (str workdir center-word-and-contexts-filename))
       )
  )

(defn train-word-vectors! [workdir]
  (let [

        batch-size 300

        ;; TODO remove uncommon tokens 843 852
        token-index (wio/nippy-thaw-from-file (str workdir token-index-filename))
        number-of-words (count token-index)

        center-word-and-contexts (wio/nippy-thaw-from-file (str workdir center-word-and-contexts-filename))




        model-filename (str workdir model-filename)

        train-data (w2v/transform-context-and-center-word-to-matrixes-2 center-word-and-contexts
                                                                        number-of-words
                                                                        )

        ]

    (with-release [fact (cudnn-factory)



                   result (w2v/train-word-vectors fact
                                                  (:average-contexts-matrix train-data)
                                                  (:center-word-matrix train-data)

                                                  (:average-contexts-matrix train-data)
                                                  (:center-word-matrix train-data)
                                                  batch-size)

                   ]
      (do
        (println "saving model to "  model-filename)
        (wio/save-matrix-to-disk! (:model result) model-filename)
        (clojure.pprint/pprint (:metrics result))
        (wio/save-list-to-disk (:metrics result) (str workdir metrics-filename ) )
        )
      )))

(defn cosine-similarity
  "returns the cosine similarity between 2 vectors "
  [u v]
  (let [dot-product (dot u v)
        norm2u (nrm2 u)
        norm2v (nrm2 v)]
    (if (some zero? [dot-product norm2u norm2v])
      0.0
      (/  dot-product norm2u norm2v))))


(defn calc-cosine [a-vec]
  (fn [index v]
    {:index index
     :cosine-similarity (cosine-similarity a-vec v)}))

(defn calc-cosine-between-one-vector-to-all-other-vectors [a-vec other-vecs]
  (let [calc-cosine-fn (calc-cosine a-vec)]
    (map-indexed calc-cosine-fn other-vecs)))

(defn get-related-words [main-word token-index-map model]
  (let [

         all-word-vectors (cols model)
        ;;all-word-vectors (rows model)

        main-word-index (get token-index-map main-word)
        main-word-vector (col model main-word-index)
       ;; main-word-vector (row model main-word-index)

        vec-index-and-cosine (calc-cosine-between-one-vector-to-all-other-vectors
                              main-word-vector all-word-vectors)


        inverted-token-index-map (s/map-invert token-index-map)

        related (reverse (sort-by :cosine-similarity vec-index-and-cosine))
        related-100 (take 100 related)

        result (map #(assoc % :word (get inverted-token-index-map (:index %)))  related-100)
        ]
    (take 10 result)
    )
  )

(defn get-related-words-with-model [word workdir]
  (let [token-index-map  (wio/nippy-thaw-from-file (str workdir token-index-filename))

        model (wio/load-matrix-from-disk (str workdir model-filename))
        ]
    (get-related-words word token-index-map model)
    )
  )
