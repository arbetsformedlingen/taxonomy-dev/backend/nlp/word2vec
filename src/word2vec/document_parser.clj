(ns word2vec.document-parser
  (:require [iota]
            [jsonista.core :as jsonista]
            [word2vec.ling :as ling]
            [clojure.core.reducers :as r]
            ))

;; Download historical jobads from jobteechdev.se
;; wget https://simonbe.blob.core.windows.net/afhistorik/pb_2006_2020.rar
;; unrar e pb_2006_2020.rar
;; cat 2020.json | jq -c '.[]' >> 2020.jsonl
;; TODO fix location from the environment variable to json file location

;; (System/getenv "JOBTECH_NLP_HISTORICAL_JOB_ADS_JSON_2017_DEV")

(def job-ads-file-location "/home/ubuntu/data/ads/ads.jsonl")

(defn load-job-ads [] (iota/seq job-ads-file-location))

(defn parse-to-json [string]
  (jsonista/read-value string jsonista/keyword-keys-object-mapper )
  )

(defn extract-header-and-text-from-job-ad-to-string [job-ad]

  (let [headline (get job-ad :headline)
        text (get-in job-ad [:description :text])
        ]
    (cond-> []
      headline (conj headline)
      text (conj text)
      ))
  )

(comment
  (def test-an-ad (jsonista/read-value (first (load-job-ads))  jsonista/keyword-keys-object-mapper ))

  ;; read json object
  ;; extract text
  ;; tokenize text
  ;; create sentences
  ;; return tokens and sentences
  ;; reduce to token-dictionary and list of all contexts
  ;; spill to disk
  ;; create matrix from token-dictionary and contexts
  ;; spill to disk?
  ;; train word vectors
  ;; save to disk
  )

;;; (count (parse-job-ads-to-tokens (take 2 (load-job-ads))))

(defn parse-job-ads-to-tokens-simple [old-jobs]
  (->> old-jobs
       (map parse-to-json)
       (mapcat extract-header-and-text-from-job-ad-to-string)
       (map ling/tokenize-text)
        (reduce (fn [acc element] (into acc element) ) [])
       )
  )

(defn conj-to-list
  ([xs x] (conj xs x))
  ([] []))

(defn parse-job-ads-to-tokens [historical-jobs]
  (->> historical-jobs
       (r/map parse-to-json)
       (r/mapcat extract-header-and-text-from-job-ad-to-string)
       (r/map ling/tokenize-text)
       (r/fold conj-to-list)
       )
  )

;; (tokens-to-token-dictionary-and-center-word-with-contexts t-tokens 3)
#_(defn tokens-to-token-dictionary-and-center-word-with-contexts [tokens context-size]
  {:token-index (ling/create-token-index tokens)
   :center-word-with-contexts (ling/create-contexts-from-sentences
                                (ling/create-sentences-from-tokens tokens) context-size)
   })

;; TODO user reducers
(defn parse-texts-to-training-data [texts]
  (ling/parse-list-of-texts-to-center-word-and-contexts (->> texts
                                                             (map parse-to-json)
                                                             (mapcat extract-header-and-text-from-job-ad-to-string)
                                                             ))
  )


;; tokenized-texts -> sentences -> flatten -> to-id -> to contex-window -> nippy

(defn tokenized-text-to-center-word-and-contexts [tokenized-text token-index]
  (let [to-index-id-fun (fn [token] (get token-index token))
        to-center-word-contexts-fun (fn [s] (ling/sentence-to-center-word-and-contexts s 3))
  ;;      sentences (mapcat ling/create-sentences-from-tokens tokenized-text)
  ;;      indexed (map #(map to-index-id-fun %) sentences)
  ;;      contexts (mapcat to-center-word-contexts-fun indexed)
        ]
    (->> tokenized-text
         (r/mapcat ling/create-sentences-from-tokens)
          (r/map #(map to-index-id-fun %))
         (r/mapcat to-center-word-contexts-fun)
        (r/fold conj-to-list)
          )
;;    contexts
    )
  )
