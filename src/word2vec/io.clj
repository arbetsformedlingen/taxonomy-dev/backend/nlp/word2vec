(ns word2vec.io
  (:gen-class)
  (:require [clojure.java.io :as jio :refer [reader]]
            [neanderthal-stick.experimental :as exp]
            [iota]
            [taoensso.nippy :as nippy]))

(defn now []
  (.format (java.text.SimpleDateFormat. "-yyyy-MM-dd-hh-mm-ss") (new java.util.Date))
  )

(defn save-matrix-to-disk! [matrix filename]
  (exp/save-to-file! matrix filename))

(defn load-matrix-from-disk [filename]
  (exp/load-from-file! filename))

(defn save-data-to-disk!
  [data filename]
  (let [_ (dorun data)]
    (nippy/freeze-to-file (jio/file filename) data)))

(defn nippy-freeze-to-file!
  [data filename]
  (let [_ (dorun data)]
    (nippy/freeze-to-file (jio/file filename) data)))

(defn nippy-thaw-from-file [filename]
  (nippy/thaw-from-file (jio/file filename)))

(defn load-data-from-disk [filename]
  (nippy/thaw-from-file (jio/file filename)))

(defn save-list-to-disk
  "Saves a list to a text file. Writes one element a row"
  [a-list filename]
  (with-open [wrtr
               (jio/writer filename :append true)]
    (binding [*out* wrtr]
      (doseq [element a-list]
        (println element)))
    ))

(defn save-lists-of-list-as-space-separated-rows
  "Saves a list to a text file. Writes one element a row"
  [a-list filename]
  (with-open [wrtr
               (jio/writer filename :append true)]
    (binding [*out* wrtr]
      (doseq [element a-list]
        (println (clojure.string/join " " element))))
    ))

(defn load-whole-file-as-list [filename]
  (with-open [r (reader filename)]
    (doall (line-seq r))))

(defn load-lazy-list [filename]
  (iota/seq filename)
  )
