(ns word2vec.ling
  (:require [clojure.set :as s]
            [clojure.string :as st]
            [clojure.core.reducers :as r]
           ))

(def test-text "Vi söker nu en skicklig clojure-utvecklare till vårt team som arbetar med klassificering och klassificeringssystem som taxonomier och ontologier över arbetsmarknaden. Rent konkret jobbar vi med de värden som används i matchningen mellan platsannonser och CV:n som till exempel yrkesbenämningar, kompetensord, förmågor och arbetsuppgifter. Klassificeringssystemen byggs upp manuellt i nära samarbete med en redaktion och maskinellt genom att bearbeta stora mängder text.")

(def test-text2
  ["Köksbiträdes sökes"
   "Kandidater söker tjänsten direkt till arbetsgivare via mail\n\n\n\nLilla Rött har som mål att alltid ha de bästa råvarorna. Vi är helt enkelt pizzerian med det lilla extra..\n\n\nDet är meriterande om du har erfarenheter sedan tidigare, men är inget krav.\n\n\nUppgifter om företaget:\n\nTulegatan 5\nSUNDBYBERG"
   "bil vårdare"
   "Natural Science teachers"
   "\nInternational company located in Stockholm is looking for a science teacher to teach around 20 hours per week.\n\nSubjects: Biology, Chemistry, Anatomy, Physics.\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
   "Entreprenadchef"
   "\nVi söker en Entreprenadchef med minst 5 års erfarenhet som kan flytande ryska och engelska i tal och skrift. Du behöver ha ekonomisk och administrativ erfarenhet.\n\nDu utgår från kontoret i norra Stockholm och har hela Sverige som arbetsområde.\n"
   "Psykolog "]
  )

(def test-sentence ["Klassificeringssystemen"
                    "byggs"
                    "upp"
                    "manuellt"
                    "i"
                    "nära"
                    "samarbete"
                    "med"
                    "en"
                    "redaktion"
                    "och"
                    "maskinellt"
                    "genom"
                    "att"
                    "bearbeta"
                    "stora"
                    "mängder"
                    "text"])

(def test-text3 "ABC 123. HAHA!\n\n Soligt? Mystik i Lund")

(def newline-token "<NEWLINE>")

(def sentence-delimiters
  #{"!" "?" "." \newline newline-token})

(def token-delimiters
  #{\! \?  \. \, \space \newline})

(defn replace-newline [c]
  (if (or (= c \newline) (= c \n) (= c "\n"))
    newline-token
    c))

(defn space? [s]
  (or (= " " s)
      (= "" s)
      )
  )

(defn add-to-tokens [tokens token]
  (conj tokens (replace-newline token))
  )

(defn delimiter? [d]
  (contains? token-delimiters (first d))
  )

(defn process-head [tokens current-token head]
  (cond
    (and (space? current-token) (space? head))  [tokens ""]

    (and (space? current-token) (not (space? head)) (delimiter? head))
      [(add-to-tokens tokens (str head)) "" ]

      (and (not (space? head)) (not (delimiter? head))
           (not (space? current-token)) (not (delimiter? current-token)))
        [tokens (str current-token head)]

      (and (not (space? head)) (not (delimiter? head))
           (not (space? current-token)) (delimiter? current-token))
      [(add-to-tokens tokens current-token) (str head)]

    (and (space? head) (delimiter? head) (not (space? current-token)))
      [(add-to-tokens tokens current-token) ""]

      (and (space? current-token) (not (space? head)) (not (delimiter? head)))
       [tokens (str head)]

    (and (delimiter? head) (not (space? head))  (not (space? current-token)))
      [(add-to-tokens tokens current-token) (str head)]
    )
  )

(defn process-empty-more [tokens current-token head]
  (if (not (space? current-token))
    (add-to-tokens tokens current-token)
    tokens
    )
  )

(defn tokenize-text [text]
  (let [lower-case-text (st/lower-case text)]
    (loop [tokens []
           current-token ""
           head (first lower-case-text)
           more (next lower-case-text)
           ]
      (let [[new-tokens new-current-token] (process-head tokens current-token (str head))
            new-head (first more)
            new-more (next more)
            ]

        (if more
          (recur new-tokens new-current-token new-head new-more)
          (process-empty-more new-tokens new-current-token new-head)
          )
        ))))

(defn count-words
  ([] {})
  ([freqs word]
    (assoc freqs word (inc (get freqs word 0)))))

(defn merge-counts
  ([] {})
  ([& m] (apply merge-with + m)))

(defn word-frequency [tokenized-text]
  (r/fold merge-counts count-words  (r/flatten tokenized-text))
  )

(defn frequent-tokens-to-index-map [frequent-tokens]
 (into {} (map #(vector (frequent-tokens %) %) (range (count frequent-tokens)))))

(defn create-token-index [tokenized-text index-size]
  (let [most-frequent-tokens
        (->> (word-frequency tokenized-text)
         (sort-by val >)
         (map #(% 0))
         (take index-size)
         (into [])
         )
        ]
    (frequent-tokens-to-index-map most-frequent-tokens)
    ))

(defn process-head-sentences-from-tokens [sentences current-sentence head]
  (if (contains? sentence-delimiters head)
    (if (not (empty? current-sentence))
      [(conj sentences current-sentence) []]
      [sentences current-sentence]
      )
    [sentences (conj current-sentence head)]
    )
  )

;; TODO use r/fold and r/foldcat
(defn create-sentences-from-tokens [tokenized-text]
  (loop [sentences []
         current-sentence []
         head (first tokenized-text)
         more (next tokenized-text)
         ]
    (let [
          [new-sentences new-current-sentence] (process-head-sentences-from-tokens sentences current-sentence head)
          new-head (first more)
          new-more (next more)
          ]
      (if more
        (recur new-sentences new-current-sentence new-head new-more)
        new-sentences
        )
      )
    )
  )

(defn token-to-id-fn [tokens]
  (fn [token]
    (.indexOf tokens token)
    )
  )

(defn sentence-tokens-to-token-index-id [sentences tokens]
  (let [fun (token-to-id-fn tokens)]
    (map #(map fun %) sentences)
    )
  )

(defn sentence-to-center-word-and-contexts [sentence half-window-size]
  (let [center-fun (fn [index word]
                     (let [[left right]   (split-at index sentence)
                           window-left    (take-last half-window-size left)
                           window-right   (take half-window-size (next right))
                           ]
                       [word (concat window-left window-right)]
                       ))]
    (map-indexed center-fun sentence)
    ))


#_(defn create-contexts-and-center-words-from-sentences [sentences half-window-size token-index]
  (let [to-token-id-fun (token-to-id-fn token-index)]
    (->> sentences
         (r/map #(map to-token-id-fun))
         (r/map #(sentence-to-center-word-and-contexts % half-window-size))
         (r/foldcat)
         ))
  )

(defn create-contexts-and-center-words-from-sentences [sentences half-window-size]
  (->> sentences
       (r/map #(sentence-to-center-word-and-contexts % half-window-size))
       (r/foldcat)
       )
  )


;; TODO refactor to use reducers
(defn parse-list-of-texts-to-center-word-and-contexts [texts]
  (let  [tokens (pmap tokenize-text texts)
         sentences (remove empty? (mapcat create-sentences-from-tokens tokens))
         token-index (vec (set (flatten sentences)))
         senteces-as-ids (sentence-tokens-to-token-index-id sentences token-index)
         ]
  (create-contexts-and-center-words-from-sentences senteces-as-ids 5)
    )
  )
