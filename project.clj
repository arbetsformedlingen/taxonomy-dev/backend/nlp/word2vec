(defproject word2vec "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [

                 [org.clojure/clojure "1.10.3"]
                 [uncomplicate/neanderthal "0.41.0"]
                 [org.bytedeco/dnnl-platform "2.1.1-1.5.5"]
                 [org.jcuda/jcudnn "11.2.0"]
                 [uncomplicate/deep-diamond "0.20.3"]
                 [iota "1.1.3"]
                 [metosin/jsonista "0.3.3"]

                 [com.fzakaria/slf4j-timbre "0.3.21"]
                 [com.taoensso/timbre "5.1.2"]


                 [com.taoensso/nippy "3.1.1"]
                 [neanderthal-stick "0.4.0"]
                 ]

  :profiles {:dev {:plugins [[lein-midje "3.2.1"]
                             [lein-codox "0.10.6"]]
                   :resource-paths ["data"]
                   :global-vars {*warn-on-reflection* true
                                 *assert* false
                                 ;;  *unchecked-math* :warn-on-boxed
                                 *print-length* 128}
                   :dependencies []}}

  :main ^:skip-aot word2vec.core
  :target-path "target/%s"

 ;; :javac-options ["-target" "1.8" "-source" "1.8" "-Xlint:-options"]
  :source-paths ["src"]

  :jvm-opts ^:replace ["-Dclojure.compiler.direct-linking=true"
                       ;; "-XX:MaxDirectMemorySize=16g"
                       "-Xmx100G"
                       "--add-opens=java.base/jdk.internal.ref=ALL-UNNAMED"]
  )
